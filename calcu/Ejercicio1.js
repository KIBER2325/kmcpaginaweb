
const numeros = document.querySelectorAll('.t');
const entrada = document.querySelector('.entrada'); 
const salida  = document.querySelector('.resultado');
const igual  = document.querySelector('.equals');
const reset  = document.querySelector('.reset');

const expresiones = {
  nro: /^[0-9/.\-\+\*\s]+$/
}
numeros.forEach((e)=>{
  e.addEventListener('click',()=>{
    entrada.textContent=entrada.textContent+e.value;
  });
});

 igual.addEventListener('click',()=>{
   salida.textContent = eval(entrada.textContent);
 });

 reset.addEventListener('click',()=>{
  entrada.textContent =' ';
  salida.textContent =' ';
 });

document.addEventListener('keydown',(event)=>{
    if (expresiones.nro.test(event.key)) {
      entrada.textContent += event.key;
    }
});





